import React from 'react';

function Pokeball({ onClick, isLoading }) {
  return (
    <div className={`Pokeball ${isLoading ? 'Pokeball--loading' : ''}`}>
      <button
        type="button"
        onClick={onClick}
        className="Pokeball__center"
      ></button>
    </div>
  );
}

export default Pokeball;
