import React, { useState, useEffect, useContext } from 'react';

import { mockList } from '../contants/mockList';
import { ListContext } from '../context/ListContext';

async function mockPromise() {
  await new Promise(resolve =>
    setTimeout(() => {
      resolve();
    }, 500)
  );

  return mockList;
}

function FilterExample() {
  const [filter, setFilter] = useState('');
  const [list, setList] = useContext(ListContext);

  useEffect(() => {
    async function getList() {
      const response = await mockPromise();
      setList(response);
    }

    getList();
  }, [setList]);

  const filteredList = list.filter(({ name }) => name.includes(filter));

  return (
    <div>
      {list.length ? (
        <div>
          <input
            type="text"
            placeholder="Filter by name"
            value={filter}
            onChange={e => setFilter(e.target.value)}
          />
          <ul>
            {filteredList.map(({ name, age }) => (
              <li key={name + age}>
                {name} - {age}
              </li>
            ))}
          </ul>
        </div>
      ) : (
        <h2>Loading list...</h2>
      )}
    </div>
  );
}

export default FilterExample;
