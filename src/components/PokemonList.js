import React from 'react';

import Pokeball from './Pokeball';

function PokemonList({ list, onClickPokeball, isLoading }) {
  return (
    <div>
      <div className="PokemonList">
        {list.map(({ id, name, imageUrl }, index) =>
          index === list.length - 1 && isLoading ? null : (
            <div key={id}>
              <p>{name}</p>
              <img src={imageUrl} alt={`Sprite of ${name}`} />
            </div>
          )
        )}
        <Pokeball onClick={onClickPokeball} isLoading={isLoading} />
      </div>
      {!list.length ? (
        <p className="PokemonList__message">No Pokémon yet...</p>
      ) : null}
    </div>
  );
}

// This makes the list render only after it's props change.
export default React.memo(PokemonList);
