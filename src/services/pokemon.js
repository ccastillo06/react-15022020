const baseUrl = 'https://pokeapi.co/api/v2/pokemon';
const getUrlWithId = id => `${baseUrl}/${id}/`;

export async function fetchPokemonById(id, successCb, errorCb = () => {}) {
  try {
    const uri = getUrlWithId(id);
    const res = await fetch(uri);
    const data = await res.json();

    const newPokemon = {
      id: data.id,
      name: data.name,
      order: data.order,
      imageUrl: data.sprites.front_default,
      type: data.types.map(({ name }) => name)
    };

    successCb(newPokemon);
  } catch (e) {
    errorCb(e.message);
  }
}
