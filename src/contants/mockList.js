export const mockList = [
  {
    name: 'juan',
    age: 20
  },
  {
    name: 'pepe',
    age: 30
  },
  {
    name: 'jhon',
    age: 40
  },
  {
    name: 'rodrigo',
    age: 50
  },
  {
    name: 'jose',
    age: 20
  },
  {
    name: 'chin',
    age: 35
  }
];
