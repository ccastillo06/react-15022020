import React, { useState, useCallback } from 'react';

import PokemonList from './components/PokemonList';
import FilterExample from './components/FilterExample';

import { useFetchPokemonById } from './hooks/usePokemon';

import { ListContextProvider } from './context/ListContext';

import './App.scss';

function App() {
  const [pokeList, setPokeList] = useState([]);

  // Create a callback function so the array of
  // dependecies using it doesn't repeat more than once.
  const addPokemon = useCallback(
    pokemon =>
      setPokeList(prev => {
        return [...prev, pokemon];
      }),
    []
  );

  // Receive values from a custom hook that fetches pokemon.
  const [{ isLoading }, increaseLastId] = useFetchPokemonById(addPokemon);

  return (
    <div className="App">
      <ListContextProvider>
        <FilterExample />
      </ListContextProvider>
      <h2>These are all the fetched pokemon!</h2>

      <PokemonList
        list={pokeList}
        onClickPokeball={increaseLastId}
        isLoading={isLoading}
      />
    </div>
  );
}

export default App;
