import React, { useState } from 'react';

export const ListContext = React.createContext();

export function ListContextProvider({ children }) {
  const [list, setList] = useState([]);

  return (
    <ListContext.Provider value={[list, setList]}>
      {children}
    </ListContext.Provider>
  );
}
