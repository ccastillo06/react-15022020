import { useEffect, useState, useCallback } from 'react';

import { fetchPokemonById } from '../services/pokemon';

export function useFetchPokemonById(successCb) {
  const [lastId, setLastId] = useState(0);
  const [isLoading, setIsLoading] = useState(false);

  useEffect(() => {
    async function handleFetchPokemonById() {
      await fetchPokemonById(lastId, successCb);
      setTimeout(() => setIsLoading(false), 500);
    }

    if (lastId) {
      handleFetchPokemonById();
    }
  }, [lastId, successCb]);

  // As the component using this hook will render in any other moment different
  // from when this hook runs, we'll make this as a callback to reference the function.
  const increaseLastId = useCallback(() => {
    setLastId(prevId => prevId + 1);
    setIsLoading(true);
  }, []);

  return [{ lastId, isLoading }, increaseLastId];
}
